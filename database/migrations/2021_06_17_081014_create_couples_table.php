<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCouplesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('couples', function (Blueprint $table) {
            $table->id();
            $table->string('hfname');
            $table->string('hlname');
            $table->string('hdateOfBirth')->nullable();
            $table->string('hemail');
            $table->string('hphone');
            $table->string('hid')->nullable();
            $table->string('himage');
            $table->string('wfname');
            $table->string('wlname');
            $table->string('wdateOfBirth')->nullable();
            $table->string('wemail')->unique();
            $table->string('wphone')->unique();
            $table->string('wid')->nullable();
            $table->string('wimage')->unique();
            $table->string('child1')->nullable();
            $table->string('child2')->nullable();
            $table->string('child3')->nullable();
            $table->string('child4')->nullable();
            $table->string('child5')->nullable();
            $table->string('child6')->nullable();
            $table->string('child7')->nullable();
            $table->string('child8')->nullable();
            $table->string('child9')->nullable();
            $table->string('child10')->nullable();
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('couples');
    }
}
