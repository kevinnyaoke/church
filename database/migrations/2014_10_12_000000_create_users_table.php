<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('fname');
            $table->string('lname');
            $table->string('gender');
            $table->date('dateOfBirth')->nullable();
            $table->string('phone')->unique();
            $table->string('email')->unique();
            $table->string('group');
            $table->string('title')->nullable();
            $table->string('marriage_status')->nullable();
            $table->string('district')->nullable();
            $table->string('password')->nullable();
            $table->string('image')->nullable();
            $table->binary('testimony')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
