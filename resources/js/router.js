import Vue from 'vue';
import VueRouter from 'vue-router';

import VueAxios from 'vue-axios';
import axios from 'axios';
Vue.use(VueAxios, axios);
// axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
// axios.defaults.baseURL = 'http://api.project.test' // Backend URL for API

import Vuesax from 'vuesax'
import 'vuesax/dist/vuesax.css' //Vuesax styles
Vue.use(Vuesax, {
  // options here
})


import Notifications from 'vue-notification'
Vue.use(Notifications)

import JwPagination from 'jw-vue-pagination';
Vue.component('jw-pagination', JwPagination);

import Home from './pages/home.vue';
import Login from './pages/login.vue';
import Register from './pages/register.vue';
import Members from './members/members.vue';
import Viewuser from './members/viewuser';
import Edituser from './members/edituser';
import editPic from './members/editPic';
import addGroup from './groups/addGroup';
import viewGroup from './groups/viewGroup';
import editGroup from './groups/editGroup';
import Couples from './couples/couples';
import regCouples from './couples/regCouples';
import Viewcouple from './couples/viewCouple';


Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    linkExactActiveClass: 'active',
    routes: [
        {
            path: '/',
            name: 'login',
            component: Login,
            meta: { hideNavigation: true }
        },
        {
            path: '/home',
            name: 'home',
            component: Home
        },
        {
            path: '/register',
            name: 'register',
            component: Register
        },
        {
            path: '/members',
            name: 'members',
            component: Members
        },
        {
            path: '/viewuser/:id/',
            name: 'viewuser',
            component: Viewuser
        },
        {
            path: '/edituser/:id/',
            name: 'edituser',
            component: Edituser
        },
        {
            path: '/user/editpic/:id/',
            name: 'editpic',
            component: editPic
        },
        {
            path: '/addgroup/',
            name: 'addgroup',
            component: addGroup
        },
        {
            path: '/viewgroup/',
            name: 'viewgroup',
            component: viewGroup
        },
        {
            path: '/editgroup/:id/',
            name: 'editgroup',
            component: editGroup
        },
        {
            path: '/couples/',
            name: 'couples',
            component: Couples
        },
        {
            path: '/register-couples/',
            name: 'regCouples',
            component: regCouples
        },
        {
            path: '/viewcouple/:id/',
            name: 'viewcouple',
            component: Viewcouple
        },
    ]
});

export default router;