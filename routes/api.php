<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
// use App\Http\Controllers\MembersController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('register', 'App\Http\Controllers\UserController@register');
Route::post('login', 'App\Http\Controllers\UserController@login');
Route::get('livesearch', 'App\Http\Controllers\UserController@livesearch');
Route::get('familysearch', 'App\Http\Controllers\UserController@familysearch');
Route::get('/viewcouple/{id}', 'App\Http\Controllers\UserController@viewcouple');
Route::get('allcouples','App\Http\Controllers\UserController@allcouples');
Route::get('/grouplist', 'App\Http\Controllers\UserController@allgroups');


Route::group([
    'middleware' => 'auth:api'
], function() {

    //user
    Route::delete('/user/delete/{id}/', 'App\Http\Controllers\UserController@deluser');
    Route::delete('/couple/delete/{id}/', 'App\Http\Controllers\UserController@delcouple');
    Route::get('married_male', 'App\Http\Controllers\UserController@married_male');
    Route::get('married_female', 'App\Http\Controllers\UserController@married_female');
    Route::get('allusers', 'App\Http\Controllers\UserController@allusers');
    
   
    Route::get('/viewuser/{id}', 'App\Http\Controllers\UserController@viewuser');
    Route::get('/edituser/{id}', 'App\Http\Controllers\UserController@edituser');
    Route::put('/user/update/{id}', 'App\Http\Controllers\UserController@updateUser');
    Route::post('/update/image/{id}', 'App\Http\Controllers\UserController@updateImage');
   
    Route::get('/storage/image/');
    Route::get('/logout','App\Http\Controllers\UserController@logout');

    //groups
    Route::get('allgroups', 'App\Http\Controllers\UserController@allgroups');
    Route::get('/editgroup/{id}', 'App\Http\Controllers\UserController@editgroup');
    Route::put('/group/update/{id}','App\Http\Controllers\UserController@updateGroup');
    Route::delete('/group/delete/{id}','App\Http\Controllers\UserController@delgroup');
    Route::post('/addgroup', 'App\Http\Controllers\UserController@addgroup');
    Route::get('/{any}', 'App\Http\Controllers\UserController@home')->where('any', '.*');
    // Route::get('/grouplist', 'App\Http\Controllers\UserController@allgroups');

    //couples
    Route::post('/regcouple', 'App\Http\Controllers\UserController@regcouple');
    Route::get('/allcouples','App\Http\Controllers\UserController@allcouples');
});