<?php

namespace App\Http\Controllers;
use App\Models\User;
use App\Models\Group;
use App\Models\Couple;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;
use Validator;
use Illuminate\Support\Facades\Auth;


use Carbon\Carbon;



class UserController extends Controller
{

    public function home(){
        return view('layouts.vue');
    }
    public function register(Request $request){

        $data = $request->validate([
            'image'   => ['image', 'mimes:jpeg,bmp,png,jpg'],
            'fname'     => ['required', 'string'],
            'lname'    => ['required', 'string'],
            'gender'     => ['required', 'string'],
            'dateOfBirth'    => ['required'],
            'district'    => ['required', 'string'],
            'phone' => ['required',],
            'email'=> ['required', 'email'],
            'group'=> ['required', 'string'],
            'title'=> ['required', 'string'],
            'marriage_status'=>['required', 'string'],
            'testimony'=> ['required', 'string'],
        ]);

        $file = $request->file('image');
        $name = '/image/' . uniqid() . '.' . $file->extension();
        $file->storePubliclyAs('public', $name);
        $data['image'] = $name;

        $user = User::create($data);
        return ['status' => true,'file'=>$file, 'message' => 'New member added successfully'];
    }

     //login
     public function login(Request $request){

        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);
        $credentials = request(['email', 'password']);
        if(!Auth::attempt($credentials))
            return ['status'=>false,'message'=>'Invalid email or password'];
        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();
        return
        [ 'message'=>'Successful login', 'status'=>true,'user'=>$user, 'access_token' => $tokenResult->accessToken,  'token_type' => 'Bearer',  'expires_at' => Carbon::parse(  $tokenResult->token->expires_at )->toDateTimeString()
        ];
    }

    public function allusers()
    {
        $admin='admin@gmail.com';
        $data = User::count();
        // return ['status' => true, 'list' => $users];
        // return response()->json($users);
        return response()->json($data); 
    }

    public function edituser($id){
        $user = User::find($id);
        return ['status' => true, 'user' => $user];
    }

    public function deluser($id)
    {
        $user = User::find($id);    
            unlink(storage_path('app/public/'.$user->image));     
        $user->delete();
       
        return ['status' => true, 'message' => 'User record has been deleted successfully'];
    }

    public function delcouple($id)
    {
        $couple = Couple::find($id);
    //    $filename = User::where('id', $id)->implode('image')->all();
    //    unlink(storage_path('app/public/'.$couple->himage));
    //    unlink(storage_path('app/public/'.$couple->wimage));
        $couple->delete();
       
        return ['status' => true, 'message' => 'Family record deleted successfully!'];
    }

    public function viewuser($id)
    {
        $user = User::find($id);
        return ['status' => true, 'user' => $user];
    }

    public function updateUser(Request $request, $id){
        $user = User::find($id);
        $user->update($request->all());


        return ['status' => true, 'message' => 'Member information has been updated successfully'];

    }

    public function updateImage(Request $request,$id){
        $data = $request->validate([
            'image'   => ['image', 'mimes:jpeg,bmp,png,jpg'],
        ]);

        $file = $request->file('image');
        $name = '/image/' . uniqid() . '.' . $file->extension();
        $file->storePubliclyAs('public', $name);
        $data['image'] = $name;
        
        $user =User::find($id);
        $user -> update($data);
        return ['status' => true, 'message' => 'User image upated successfully'];
    }

    public function viewcouple($id)
    {
        $user = Couple::find($id);
        return ['status' => true, 'couple' => $user];
    }

    //logout
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return ['status' => true, 'message' => 'You are are Logged out!'];
    }

   public function addgroup(Request $request){
      $group=Group::create($request->all());
      return ['status' => true, 'message' => 'New group has been added successfully'];
   }

   public function allgroups(){
       $group=Group::all();
       return ['status' => true, 'list' => $group];
   }

   public function editgroup($id){
     $group = Group::find($id);
     return ['status' => true, 'group' => $group];
   }

   public function updateGroup(Request $request, $id){
    $group = Group::find($id);
    $group->update($request->all());
   return['status'=>true,'message'=>'Group name has been updated successfully'];
  }

  public function delgroup($id){
    $group = Group::find($id);
    $group->delete();
    return['status'=>true, 'message'=>'Group name has been updated successfully'];      
  }

  public function regcouple(Request $request){

    $request->validate([
        'husb_phone' => 'required',
        'wife_phone' => 'required',
    ]);

     $husb=User::all()->where('phone','==',$request->husb_phone); 
     $wife=User::all()->where('phone','==',$request->wife_phone); 

           if ($husb->isEmpty()) {
            return ['status' => 'no-husb', 'message' => 'Husband Contact is not in our records'];
           
           }elseif ($wife->isEmpty()) {
            return ['status' => 'no-wife', 'message' => 'Wife Contact is not in our records'];
           }
           else {
            Couple::create([                
                'hfname'=>$husb->implode('fname'),
                'hlname'=>$husb->implode('lname'),
                'hdateOfBirth'=>$husb->implode('dateOfBirth'),
                'hemail'=>$husb->implode('email'),
                'hphone'=>$husb->implode('phone'),
                // 'hid'=>$husb->implode('fname'),
                'himage'=>$husb->implode('image'),

                'wfname'=>$wife->implode('fname'),
                'wlname'=>$wife->implode('lname'),
                'wdateOfBirth'=>$wife->implode('dateOfBirth'),
                'wemail'=>$wife->implode('email'),
                'wphone'=>$wife->implode('phone'),
                // 'wid'=>$wife->implode('fname'),
                'wimage'=>$wife->implode('image'),

                'child1'=>$request->input('child1'),
                'child2'=>$request->input('child2'),
                'child3'=>$request->input('child3'),
                'child4'=>$request->input('child4'),
                'child5'=>$request->input('child5'),
                'child6'=>$request->input('child6'),
                'child7'=>$request->input('child7'),
                'child8'=>$request->input('child8'),
                'child9'=>$request->input('child9'),
                'child10'=>$request->input('child10')

            ]);

              return ['status' => 'true', 'message' => 'New Family has been created successfully'];
           }
    }

//   public function regcouple(Request $request){

//     $data = $request->validate([    
        
//         'hfname'     => ['required', 'string'],
//         'hlname'    => ['required', 'string'],
//         'hemail'=> ['required', 'email'],
//         'hdateOfBirth'    => ['required'],
//         'hphone'    => ['required', 'string'],
//         'hid'    => ['required', 'string'],
//         'himage'   => ['image', 'mimes:jpeg,bmp,png,jpg'],
//         'wfname'     => ['required', 'string'],
//         'wlname'    => ['required', 'string'],
//         'wdateOfBirth'    => ['required'],
//         'wemail'=> ['required', 'email'],
//         'wphone'    => ['required', 'string'],
//         'wid'    => ['required', 'string'],
//         'wimage'   => ['image', 'mimes:jpeg,bmp,png,jpg'],
//         'child1'    => ['string'],
//         'child2'    => ['string'],
//         'child3'    => ['string'],
//         'child4'    => ['string'],
//         'child5'    => ['string'],
//         'child6'    => ['string'],
//         'child7'    => ['string'],
//         'child8'    => ['string'],
//         'child9'    => ['string'],
//         'child10'    => ['string'],
//     ]);

//     $file = $request->file('himage');
//     $hname = '/image/' . uniqid() . '.' . $file->extension();
//     $file->storePubliclyAs('public', $hname);
//     $data['himage'] = $hname;

//     $file = $request->file('wimage');
//     $wname = '/image/' . uniqid() . '.' . $file->extension();
//     $file->storePubliclyAs('public', $wname);
//     $data['wimage'] = $wname;

//     $user = Couple::create($data);
//     return ['status' => true, 'message' => 'Family Created Successfully'];
//   }

  public function allcouples(){
    $couples = Couple::all();
    return ['status' => true, 'list' => $couples];
  }

  public function married_male(){
    $married_male = User::all()->where('marriage_status','==', 'Married')->where('gender','==','Male');
    // $lastname = User::all()->where('marriage_status','==', 'Married')->where('gender','==','Male')->implode('lname');
    // $data = CONCAT($firstname,' ',$lastname);
    // $users = User::select("*", \DB::raw("CONCAT(users.fname,' ',users.lname) as full_name"))
    //     ->implode('full_name');
    return ['status' => true, 'list' => $married_male];
  }

  public function married_female(){
    $married_female = User::all()->where('marriage_status','==', 'Married')->where('gender','==','Female');
    return ['status' => true, 'list' => $married_female];
  }

  public function livesearch(Request $request){
    $data = \DB::table('users')->Where('fname', 'LIKE','%'.$request->keyword.'%')
    ->orWhere('lname', 'LIKE','%'.$request->keyword.'%') 
    ->orWhere('phone', 'LIKE','%'.$request->keyword.'%') 
    ->orWhere('district', 'LIKE','%'.$request->keyword.'%')->get();
    // if($data->isEmpty()){
    //    return ['status'=>'blank', 'message'=>'No member with such details'];
    // }else{
      return response()->json($data); 
    
  }

  public function familysearch(Request $request){
    $data = Couple::Where('hfname', 'LIKE','%'.$request->keyword.'%')
    ->orWhere('hlname', 'LIKE','%'.$request->keyword.'%') 
    ->orWhere('wfname', 'LIKE','%'.$request->keyword.'%') 
    ->orWhere('wlname', 'LIKE','%'.$request->keyword.'%')->get();
    // ->orWhere('phone', 'LIKE','%'.$request->keyword.'%') 
    // ->orWhere('district', 'LIKE','%'.$request->keyword.'%') ;
    return response()->json($data); 
  }
}
