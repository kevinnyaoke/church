<?php

namespace App\Http\Controllers;
use App\Models\Members;
use Illuminate\Http\Request;

class MembersController extends Controller
{
    public function addmember(Request $request){
        $data = Members::create($request->all());
        return ['status' => true, 'message' => 'New member added successfully!'];
    }
}
