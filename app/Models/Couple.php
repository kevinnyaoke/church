<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Couple extends Model
{
    use HasFactory;

    protected $fillable=[
        'hfname',
        'hlname',
        'hdateOfBirth',
        'hemail',
        'hphone',
        'hid',
        'himage',
        'wfname',
        'wlname',
        'wdateOfBirth',
        'wemail',
        'wphone',
        'wid',
        'wimage',
        'child1',
        'child2',
        'child3',
        'child4',
        'child5',
        'child6',
        'child7',
        'child8',
        'child9',
        'child10',
    ];
}